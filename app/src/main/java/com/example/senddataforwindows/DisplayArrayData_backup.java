package com.example.senddataforwindows;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class DisplayArrayData_backup extends AppCompatActivity {
  private File file;
  private String path;
  private String fileName;
  private Button button1;
  public String[] array;
  public String state;
  private Context cont;

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_display_array_data);

    this.fileName = "test.txt";
    this.path = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString();
    System.out.println(cont);
    this.file = new File(cont.getFilesDir(), fileName);
    this.button1 = findViewById(R.id.button_save);
    System.out.println(this.file);
    System.out.println(this.path);

    //MainActivityからデータの受け取り
    Bundle extras = getIntent().getExtras();
    array = extras.getStringArray("test");
    System.out.println(array);
    ArrayList<String> test = new ArrayList<String>(Arrays.asList(array));
    System.out.println(test);

    // リスト項目とListViewを対応付けるArrayAdapterを用意する
    ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, test);
    // ListViewにArrayAdapterを設定する
    ListView listView = (ListView) findViewById(R.id.listView);
    listView.setAdapter(adapter);
    setEvent();
  }


  private void setEvent() {
    button1.setOnClickListener(new View.OnClickListener() {
      @RequiresApi(api = Build.VERSION_CODES.O)
      @Override
      public void onClick(View v) {
        String text = String.join("\n", array);
        state = Environment.getExternalStorageState();
        System.out.println(Environment.MEDIA_MOUNTED.equals(state));
        if (Environment.MEDIA_MOUNTED.equals(state)) {
          System.out.println(file);

          saveFile(text);
        }
      }
    });
  }


  private void saveFile(String str) {
    // try-with-resources
    try (FileWriter writer = new FileWriter(file)) {
      writer.write(str);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}
