package com.example.senddataforwindows;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    System.out.println("created");
  }
  public void sendMessage(View view) {
    //Intentでクラス名：DisplayArrayDataを呼び出す
    Intent intent = new Intent(this, DisplayArrayData.class);
    //dataにバーコードを追加していく
    String arr[] = {"Z", "O", "T", "aaa"};
//    data.add("001");
//    data.add("002");
//    data.add("003");
//    data.add("004");
//    data.add("005");
//    data.add("006");
//    data.add("007");
//    data.add("008");
//    data.add("009");
    System.out.println("data追加完了");
    //dataをクラス名：DisplayArrayDataに渡す
    intent.putExtra("test", arr);
    //DisplayArrayDataの起動？
    startActivity(intent);
    System.out.println("data送信完了");
  }

}