package com.example.senddataforwindows;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static android.icu.text.DateTimePatternGenerator.PatternInfo.OK;
import static java.security.AccessController.getContext;

public class DisplayArrayData extends AppCompatActivity {
  private String file;
  private String path;
  public String state;
  private String fileName;
  private Button button1;
  public String[] array;
  private accessClass testTask;
  private final String[] PERMISSIONS = {
          Manifest.permission.WRITE_EXTERNAL_STORAGE
  };
  private final int REQUEST_PERMISSION = 1000;

  @RequiresApi(api = Build.VERSION_CODES.N)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_display_array_data);
    this.button1 = findViewById(R.id.button_save);
    Date d = new Date();
    SimpleDateFormat d1 = new SimpleDateFormat("yyyyMMddHHmmss");
    String c1 = d1.format(d);

    this.fileName = String.format("test_"+c1+".txt");
    this.path = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString();
    this.file = this.path + "/" + this.fileName;

    //MainActivityからデータの受け取り
    Bundle extras = getIntent().getExtras();
    array = extras.getStringArray("test");
    ArrayList<String> test = new ArrayList<String>(Arrays.asList(array));

    // リスト項目とListViewを対応付けるArrayAdapterを用意する
    ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, test);
    // ListViewにArrayAdapterを設定する
    ListView listView = (ListView) findViewById(R.id.listView);
    listView.setAdapter(adapter);

    checkPermission();
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  private void checkPermission(){
    if (isGranted()){
      setEvent();
    }
    else {
      requestPermissions(PERMISSIONS, REQUEST_PERMISSION);
    }
  }


  @RequiresApi(api = Build.VERSION_CODES.M)
  private boolean isGranted(){
    for (int i = 0; i < PERMISSIONS.length; i++){
      //初回はPERMISSION_DENIEDが返る
      if (checkSelfPermission(PERMISSIONS[i]) != PackageManager.PERMISSION_GRANTED) {
        //一度リクエストが拒絶された場合にtrueを返す．初回，または「今後表示しない」が選択された場合，falseを返す．
        if (shouldShowRequestPermissionRationale(PERMISSIONS[i])) {
          Toast.makeText(this, "アプリを実行するためには許可が必要です", Toast.LENGTH_LONG).show();
        }
        return false;
      }
    }
    return true;
  }


  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == REQUEST_PERMISSION){
      checkPermission();
    }
  }


  private void setEvent(){
    button1.setOnClickListener(new View.OnClickListener() {
      @RequiresApi(api = Build.VERSION_CODES.O)
      @Override
      public void onClick(View v) {
        String text = String.join("\n", array);
//        String text = editText.getText().toString();
        state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)){
          // ボタンをタップして非同期処理を開始
          saveFile(file, text);
        }
      }
    });
  }


  private void saveFile(String file, String str){
    try {
      FileOutputStream fileOutputStream = new FileOutputStream(file, false);
      fileOutputStream.write(str.getBytes());
      testTask = new accessClass();
      testTask.execute(str);
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setTitle("タイトル");
      builder.setMessage("作成完了しました。");
      builder.setPositiveButton("OK", null);
      AlertDialog dialog = builder.create();
      dialog.show();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  private static final String ACTION_USB_PERMISSION =
          "com.android.example.USB_PERMISSION";
  private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {

    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      if (ACTION_USB_PERMISSION.equals(action)) {
        synchronized (this) {
          UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

          if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
            if(device != null){
              //call method to set up device communication
            }
          }
          else {
//            Log.d(TAG, "permission denied for device " + device);
          }
        }
      }
    }
  };
}
