package com.example.senddataforwindows;

import org.apache.log4j.Appender;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.Properties;

import jcifs.CIFSContext;
import jcifs.CIFSException;
import jcifs.config.PropertyConfiguration;
import jcifs.context.BaseContext;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.NtlmPasswordAuthenticator;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

import static java.sql.DriverManager.println;

public class JCIFSTest {
  final String userName="shu.morinaka";
  final String passWord = "hnde8869";
  final String remoteFile = "smb:\\\\192.168.1.33\\share\\test.txt";
  SmbFile smbFile = null;

/**
 * 接続用プロパティをセットする
 * @param p プロパティ
 */
    private void setProperties(Properties p){
      p.setProperty("jcifs.smb.client.minVersion", "SMB202");
      p.setProperty("jcifs.smb.client.maxVersion", "SMB311");
    }

/**
 * 共有エリアに接続する
 * @param auth 認証情報
 * @param uncPath 接続先のUNCパス
 * @return 共有ファイル情報
 */
    private SmbFile connect(CIFSContext auth, String uncPath ) {
      try {
        String url = uncPath.replace("\\", "/");
        return new SmbFile(url, auth);
      } catch( MalformedURLException e){
        e.printStackTrace();
        return null;
      }
    }

    void setup(String str){
      //接続用プロパティを作成する
      Properties prop = new Properties();
      setProperties(prop);

      try {
        //接続情報を作成する
        BaseContext baseContext = new BaseContext(new PropertyConfiguration(prop));
        NtlmPasswordAuthenticator authenticator = new NtlmPasswordAuthenticator(userName, passWord);
        CIFSContext cifsContext = baseContext.withCredentials(authenticator);

        //接続する
        SmbFile smbFile = connect(cifsContext, remoteFile);
        if(smbFile == null) return;

//        String readPath = str; //ローカルの元ファイル

        //読み書き用ストリームを用意する
        BufferedInputStream bis = null;
        SmbFileOutputStream os = smbFile.openOutputStream();

        try {
//          println( readPath + "を"
//                  + smbFile.getCanonicalUncPath() + "に複写します");

//          元ファイルをOPENする
//          bis = new BufferedInputStream(new FileInputStream(readPath));

          //順次読みだして共有エリアに書き込む
//          byte[] bytes = new byte[1024];
//          int len = 0;
//          while ((len = bis.read(bytes, 0, bytes.length)) != -1) {
//            //書き込む
//            os.write( bytes );
//          }
          os.write(str.getBytes());
          os.flush();
          os.close();
          println( "複写しました");
        } catch( FileNotFoundException e){
          e.printStackTrace();
        } catch( IOException e){
          e.printStackTrace();
        } finally {
          if( bis != null ){
            try{
              bis.close();
            } catch( IOException e){
              e.printStackTrace();
            }
          }
        }
      } catch (CIFSException e) {
        e.printStackTrace();
      } finally {
        if( smbFile != null ){
          smbFile.close();
        }
      }
    }
  }